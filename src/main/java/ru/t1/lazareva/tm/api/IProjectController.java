package ru.t1.lazareva.tm.api;

public interface IProjectController {

    void showProjects();

    void createProject();

    void clearProjects();
}