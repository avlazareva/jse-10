package ru.t1.lazareva.tm.api;

public interface ICommandController {

    void showErrorArgument();

    void showSystemInfo();

    void showErrorCommand();

    void showVersion();

    void showAbout();

    void showHelp();

}