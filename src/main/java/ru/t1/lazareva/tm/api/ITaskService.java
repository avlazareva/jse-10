package ru.t1.lazareva.tm.api;

import ru.t1.lazareva.tm.model.Task;

public interface ITaskService extends ITaskRepository {
    Task create(String name, String description);

}