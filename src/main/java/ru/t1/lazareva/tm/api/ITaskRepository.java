package ru.t1.lazareva.tm.api;

import ru.t1.lazareva.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    void clear();

    List<Task> findAll();

}