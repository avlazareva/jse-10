package ru.t1.lazareva.tm.api;

import ru.t1.lazareva.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}