package ru.t1.lazareva.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();
}