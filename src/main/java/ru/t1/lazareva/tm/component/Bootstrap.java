package ru.t1.lazareva.tm.component;

import ru.t1.lazareva.tm.api.*;
import ru.t1.lazareva.tm.constant.ArgumentConst;
import ru.t1.lazareva.tm.constant.CommandConst;
import ru.t1.lazareva.tm.controller.CommandController;
import ru.t1.lazareva.tm.controller.ProjectController;
import ru.t1.lazareva.tm.controller.TaskController;
import ru.t1.lazareva.tm.repository.CommandRepository;
import ru.t1.lazareva.tm.repository.ProjectRepository;
import ru.t1.lazareva.tm.repository.TaskRepository;
import ru.t1.lazareva.tm.service.CommandService;
import ru.t1.lazareva.tm.service.ProjectService;
import ru.t1.lazareva.tm.service.TaskService;
import ru.t1.lazareva.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void processCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArguments(arguments[0]);
        exit();
    }

    private void exit() {
        System.exit(0);
    }

    private void processCommand(final String argument) {
        switch (argument) {
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.EXIT:
                exit();
            default:
                commandController.showErrorCommand();
        }
    }

    private void processArguments(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    public void run(final String... args) {
        processArguments(args);
        processCommands();
    }

}